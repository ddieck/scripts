#!/usr/bin/perl -w

use strict;
use LWP::UserAgent;
use Data::Dumper;
use JSON;
use GD::Graph::lines;
use Getopt::Long;

my ( $user, $repo );
GetOptions( 'repo=s' => \$repo, 'user=s' => \$user );
if ( !$user || !$repo) {
	die "--user and --repo must be defined!";
}

# my $url = "https://api.bitbucket.org/2.0/repositories/ddieck/vortrag_psych_krankh/commits";
my $url = "https://api.bitbucket.org/1.0/repositories/$user/$repo/changesets";

my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
my $res = $ua->get($url);
if ( $res->{_rc} != 200 ) {
	die "Could not fetch data from $url: $!";
}

my $json = decode_json($res->{_content});
my %times;
foreach (@{$json->{changesets}}) {
	my $msg = $_->{message};
	$msg =~ s/\n//;
	print "$_->{timestamp}\t" . $msg . "\n";
	my @clock = split(' ', $_->{timestamp});
	my @foo = split(':', $clock[1]);
	$times{$foo[0]}++;
}

my @data = ([], []);
foreach ( sort keys %times ) {
	push $data[0], $_;
	push $data[1], $times{$_}
}

my $graph = new GD::Graph::lines();
$graph->set(
	x_label => 'Hour',
	y_label => 'Commits',
);

$graph->plot(\@data);
open (my $fh, '>', "graph.png") or die $!;
binmode $fh;
print $fh $graph->gd->png;
close $fh;
# print Dumper(\@data);
